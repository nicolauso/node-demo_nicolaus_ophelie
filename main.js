// Crée un script JavaScript contenant la validation d'un tableau d'adresses email
// Il s'agira d'afficher chaque adresse email du tableau, et d'afficher un booléen pour préciser si l'adresse est valide ou non
// Il doit être possible d'exécuter le projet avec la commande npm start

import chalk from 'chalk';
import validator from 'validator';
const log = console.log;

// Combine styled and normal strings
log(chalk.blue('Hello') + ' World' + chalk.red('!'));

let myEmailAdress= "n.ophelie@gmail.com"
let arrayOfEmails = [
    'jeteste@gmail.com', 'jeNeSuisPasUnEmail', 'laurence.b@outlook.com', 'jeFaisSemblantdEtreUnMail@', 'mairie.toulouse@tlse-mairie.fr'
]
// Fonction qui vérifie que l'input est bien un email 
// Utilisation de la librairie validatorjs avec la méthode isEmail(string[,options])
const isValidateEmail = (email) => validator.isEmail(email) 
let resultOFValidateEmail = isValidateEmail(myEmailAdress);

// Application de la fonction de validation du type Email à chaque élément d'un talbeau avec .map()
let testAllArrayOfMails = 
arrayOfEmails.map((email) => isValidateEmail(email));

console.log("testAllArrayOfMails", testAllArrayOfMails);


//console.log(resultOFValidateEmail);

